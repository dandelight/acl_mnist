#include <cstdio>
#include <cstdlib>
#define CHECK(call)                                                        \
  do {                                                                     \
    const aclError error_code = call;                                      \
    if (error_code != ACL_SUCCESS) {                                       \
      fprintf(stderr, "\033[1;31mError in %s, file %s, line %d\033[0m\n",  \
              #call, __FILE__, __LINE__);                                  \
      fprintf(stderr, "\033[1;31mError code: %d\033[0m\n", error_code);    \
      fprintf(stderr, "\033[1;31mError message: %s\033[0m\n",              \
              aclGetRecentErrMsg());                                       \
      exit(0);                                                             \
    } else {                                                               \
      fprintf(stderr, "\033[1;32mASCEND API " #call " succeed!\033[0m\n"); \
    }                                                                      \
  } while (0)

#define CHECK_NULLPTR(pointer)                                               \
  do {                                                                       \
    if (pointer == nullptr && aclGetRecentErrMsg() == nullptr) {             \
      fprintf(stderr, "\033[1;31mPointer " #pointer " is nullptr\033[0m\n"); \
      fprintf(stderr, "\033[1;31mError message: %s\033[0m\n",                \
              aclGetRecentErrMsg());                                         \
      exit(0);                                                               \
    } else {                                                                 \
      fprintf(stderr,                                                        \
              "\033[1;32mPointer " #pointer                                  \
              " now is pointing at %#llX!\033[0m\n",                         \
              (unsigned long long)pointer);                                  \
    }                                                                        \
  } while (0)
